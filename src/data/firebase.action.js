import { firebase } from "@firebase/app";
import "@firebase/firestore";
import "@firebase/auth";
import { post_action, auth_action, notification_action } from "../store";
import { firebase_validate } from "./firebase.validate";

export const firebase_action = {
  subscribeAuthChanges() {
    firebase.auth().onAuthStateChanged(userInfo => {
      if (userInfo) {
        auth_action.sign_in(userInfo);
        this.subscribePostChanges(userInfo.uid);
      } else {
        auth_action.sign_out();
      }
    });
  },
  async subscribePostChanges(userId) {
    try {
      const collection_path = `users/${userId}/posts`;
      const ref = firebase.firestore().collection(collection_path);
      ref.onSnapshot(snapshot => snapshot.docChanges().forEach(onPostChange));
    } catch (err) {
      error_store_action.set_error_message("Error reading user posts", 6000);
    }
  },
  signIn() {
    const provider = new firebase.auth.GoogleAuthProvider();
    return firebase.auth().signInWithPopup(provider);
  },
  signOut() {
    return firebase.auth().signOut();
  },
  async savePost(post, userId) {
    try {
      firebase_validate.validate_post(post);
      const collection_path = `users/${userId}/posts`;
      const db = firebase.firestore().collection(collection_path);

      if (post.id) {
        await db.doc(post.id).update(post);
        return
      }
      const result = await db.add(post);
      return result

    } catch (err) {
			notification_action.add_error("Error saving posts", "error");
		}
  },
  removePost(postId, userId) {
		try {
			const doc_path = `users/${userId}/posts/${postId}`;
	    const ref = firebase.firestore().doc(doc_path);
			return ref.delete();
		} catch(err) {
			notification_action.add_error("Error removing posts");
		}
  }
};

function onPostChange(change) {
  const post = change.doc.data();
  const id = change.doc.id;

  switch (change.type) {
    case "added":
    case "modified":
      return post_action.add_modify_post(id, post);
    case "removed":
      return post_action.remove_post(id);
    default:
      return;
  }
}
