import { firebase } from "@firebase/app"
import "@firebase/firestore"
import "@firebase/auth"
import firebaseConfig from "./firebase.config"
import { firebase_action } from './firebase.action'

firebase.initializeApp(firebaseConfig)

firebase_action.subscribeAuthChanges()