const MAX_TITLE_CHAR = 70


export const firebase_validate = {

  validate_post(post) {
    let title = post.title ? post.title.trim() : "";
    let markdown = post.markdown ? post.markdown.trim() : "";

    if (!title || !markdown) {
      return "Post title and markdown required";
    }
    if (title.length > MAX_TITLE_CHAR) {
      return `Post titile max of ${MAX_TITLE_CHAR} characters`;
    }
  }
};
