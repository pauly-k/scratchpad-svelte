import App from './App.svelte';
import './data/firebase.init'

const app = new App({
	target: document.body
});

export default app;