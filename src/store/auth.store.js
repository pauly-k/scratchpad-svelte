let initial_state = {
    authenticated: false,
    uid: null,
    displayName: "",
    email: "",
    photoURL: ""
}

export function create_auth_action(store) {

  if (!store.user) {
    store.update(store => {
      store.user = initial_state
      return store
    })
  }

  function sign_in(userInfo) {
    store.update(store => {
      let { uid, displayName, email, photoURL } = userInfo
      store.user = { uid, displayName, email, photoURL, authenticated: true }
      return store
    })
  }

  function sign_out() {
    store.update(store => {      
      store.user = { authenticated: false }
      store.posts.all = []
      return store
    })
  }

  return {
    sign_in,
    sign_out
  }
}

