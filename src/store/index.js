import { writable } from "svelte/store";
import { create_auth_action } from "./auth.store";
import { create_post_action } from "./post.store";
import { create_notification_action } from "./notification.store";

export const store = writable({});
export const auth_action = create_auth_action(store);
export const post_action = create_post_action(store);
export const notification_action = create_notification_action(store);
