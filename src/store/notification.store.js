let initial_state = {
  watcher: null,
  list: [],
  duration: 4000
};

export function create_notification_action(store) {
  if (!store.notification) {
    store.update(store => {
      store.notifications = initial_state
      return store
    })
  }

  function remove_expired() {
    store.update(store => {
      let list = store.notifications.list;
      if (list.length > 0) {
        list = [...list.filter(item => Date.now() < item.expiration)];
        store.notifications.list = list;

        let watcher = store.notifications.watcher;
        if (watcher && list.length === 0) {
          clearInterval(watcher);
          store.notifications.watcher = null;
        }
      }
      return store;
    });
  }

  function add_notification(message, notification_type) {
    store.update(store => {
      let duration = store.notifications.duration;
      let list = store.notifications.list;

      list = [...list, { 
        message, 
        type: notification_type,
        expiration: Date.now() + duration 
      }];

      store.notifications.list = list;

      let watcher = store.notifications.watcher;
      if (!watcher) {
        let duration = store.notifications.duration;
        watcher = setInterval(remove_expired, duration);
        store.notifications.watcher = watcher;
      }
      return store;
    });
  }

  function add(text) {
    add_notification(text)
  }

  function add_error(text) {
    add_notification(text, "error")
  }

  return {
    add,
    add_error
  };
}
