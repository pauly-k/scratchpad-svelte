let initial_state = {
    all: [],
    selected: null
}

export function  create_post_action(store) {

  if (!store.posts) {
    store.update(store => {
      store.posts = initial_state
      return store
    })
  }

  function add_modify_post(id, post) {
    store.update(store => {
      post.id = id
      let posts = store.posts.all || []
      let index = posts.findIndex(post => post.id === id)
      if (index >= 0) {
        store.posts.all = [
          ...posts.slice(0, index), 
          post, 
          ...posts.slice(index + 1)
        ]
      } else {
        store.posts.all = [...posts, post]
      }
      return store
    })
  }
  function remove_post(id) {		
    store.update(store => {
      let posts = store.posts.all || []
      let index = posts.findIndex(post => post.id === id)
      if (index >= 0) {
        store.posts.all = [posts.slice(0, index), posts.slice(index + 1)]
      }
      return store
    })
  }

  return {
    add_modify_post,
    remove_post
  }
}

